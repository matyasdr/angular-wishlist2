import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from "./../models/destino-viaje.model"

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  //destinos: string[];
  destinos: DestinoViaje[];
  constructor() {
    //this.destinos = ["Barranquilla","Lima","Buemos Aires","Barcelona"];
    this.destinos = []; //inicializa array vacio
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    //console.log(new DestinoViaje(nombre, url)); //console.log(nombre);
    //console.log(url);
    console.log(this.destinos);
    return false; //para que no recargue pagina luego del clic, propio de javascript sobre eventos
  }
}
